import { Component, ViewChild, ViewEncapsulation, Inject, OnInit, ElementRef } from '@angular/core';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxinput';
import { jqxButtonComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons';

import { Message } from '../message';
import { MessagesHelperService } from '../messageshelper.service';
import { Note } from '../note';
import { NoteService } from '../note.service';

import * as moment from 'moment';

import alertify from 'alertifyjs/build/alertify.min.js';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from '../app.config';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css'],  
  encapsulation: ViewEncapsulation.None,
  providers: [{ provide: APP_CONFIG, useValue: APP_DI_CONFIG }]
})
export class MessagesComponent implements OnInit {  

  @ViewChild('messagesGrid') messagesGrid: jqxGridComponent;
  @ViewChild('messagesFindDialog') messagesFindDialog: jqxWindowComponent;
  @ViewChild('messagesFindDialogDropDownList') messagesFindDialogDropDownList: jqxDropDownListComponent;
  @ViewChild('messagesFindDialogInput') messagesFindDialogInput: jqxInputComponent;
  
  statuses: any = null;

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private elementRef: ElementRef,  
    private messagesHelperService: MessagesHelperService,
    private noteService: NoteService
  ) {    
    moment.locale('ru');   
  }

  ngOnInit() {
    this.loadScript('../../assets/components/audioplayer/js/audioplayer.js');    
    this.loadScript('../../assets/components/audioplayer/js/init.js');

    this.getStatuses();
  }

  getStatuses() {
    this.messagesHelperService.getStatuses()
      .then(statuses => {
        this.statuses = statuses;        

        this.statusesNote = this.createStatusesDataAdapter();
        this.messagesGrid.source(this.createMessagesDataAdapter());            
      })
      .catch(err => {
        console.error(err);
        alertify.error(err.message);
      }); 
  }

  createStatusesDataAdapter(): any {
    let statuses = this.statuses && this.statuses.map(x => Object.assign({}, x)) || [];

    let statusesSource: any =
    {
      datatype: 'array',
      datafields: [
        { name: 'id', type: 'number' },
        { name: 'name', type: 'string' }
      ],
      localdata: statuses
    };

    return new jqx.dataAdapter(statusesSource, { autoBind: true });;
  }

  statusesNote: any = this.createStatusesDataAdapter();

  getStatusIdByName(name: string): number {
    let list = this.statuses && this.statuses.map(x => Object.assign({}, x)) || [];
    for (let i = 0; i < list.length; i++) {
      let currentItem = list[i];
      if (currentItem.name === name) {
        return currentItem.id;
      }
    }

    return null;
  }

  lastResizeEventTimeStamp: any = null;
  onResize(event: any): void {
    if (this.lastResizeEventTimeStamp && event.timeStamp - this.lastResizeEventTimeStamp < 500) {    
      event.stopPropagation();    
      return;
    }
   
    this.lastResizeEventTimeStamp = event.timeStamp;

    let intervalId_ = setInterval(()=>{
      if ((window as any).initAudioPlayerAll) {
        (window as any).initAudioPlayerAll();
        clearInterval(intervalId_);
      }
    }, 500);
  }

  private loadScript(url) {
    let scriptEl = document.createElement('script');
    scriptEl.type = 'text/javascript';
    scriptEl.src = url;
    scriptEl.async = false;
    this.elementRef.nativeElement.appendChild(scriptEl);
  }

  initMessagesButtons() {    
    let newEl = document.createElement('i');
    newEl.className = 'fas fa-plus-circle';
    let targetEl = document.querySelector('#addMessageButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let newMessage: Message = new Message();
      this.messagesGrid.addrow(null, newMessage);
      this.messagesGrid.selectrow(parseInt(this.messagesGrid.getdatainformation().rowscount) - 1);
    });
    (targetEl as HTMLElement).style.display = 'none';

    newEl = document.createElement('i');
    newEl.className = 'fas fa-minus-circle';
    targetEl = document.querySelector('#deleteMessageButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let messagesGridSelectRowIndex = this.messagesGrid.getselectedrowindex();
      let messagesGridRowsCount = this.messagesGrid.getdatainformation().rowscount;
      if (messagesGridSelectRowIndex >= 0 && messagesGridSelectRowIndex < parseFloat(messagesGridRowsCount)) {
        let id = this.messagesGrid.getrowid(messagesGridSelectRowIndex);
        let data = this.messagesGrid.getrowdata(messagesGridSelectRowIndex);
      }
      else {
        alertify.alert('Информация', 'Действие может быть выполнено только для текущего сообщения. Сначала нужно выбрать строку, которая устанавливается как активное сообщение.');
      }
    });
    (targetEl as HTMLElement).style.display = 'none';
    
    newEl = document.createElement('i');
    newEl.className = 'fas fa-sync-alt';
    targetEl = document.querySelector('#reloadMessageButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      this.messagesGrid.source(this.createMessagesDataAdapter());
    });
   
    newEl = document.createElement('i');
    newEl.className = 'fas fa-search';
    targetEl = document.querySelector('#searchMessageButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {      
      this.messagesFindDialog.open();
      this.messagesFindDialog.move(60, 60);
    });
  }
  
  createMessagesButtonsContainers(statusbar: any): void {    
    let buttonsContainer = document.createElement('div') ;
    buttonsContainer.style.cssText = 'overflow: hidden; position: relative; margin: 5px;';

    let addMessageButtonContainer = document.createElement('div');
    let deleteMessageButtonContainer = document.createElement('div');
    let reloadMessageButtonContainer = document.createElement('div');
    let searchMessageButtonContainer = document.createElement('div');

    addMessageButtonContainer.id = 'addMessageButton';
    deleteMessageButtonContainer.id = 'deleteMessageButton';
    reloadMessageButtonContainer.id = 'reloadMessageButton';
    searchMessageButtonContainer.id = 'searchMessageButton';

    addMessageButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    deleteMessageButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    reloadMessageButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    searchMessageButtonContainer.style.cssText = 'float: left; margin-left: 5px;';

    buttonsContainer.appendChild(addMessageButtonContainer);
    buttonsContainer.appendChild(deleteMessageButtonContainer);
    buttonsContainer.appendChild(reloadMessageButtonContainer);
    buttonsContainer.appendChild(searchMessageButtonContainer);

    statusbar[0].appendChild(buttonsContainer);

    let addMessageButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'      
    }
    let addMessageButton = jqwidgets.createInstance('#addMessageButton', 'jqxButton', addMessageButtonOptions);

    let deleteMessageButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'
    }
    let deleteMessageButton = jqwidgets.createInstance('#deleteMessageButton', 'jqxButton', deleteMessageButtonOptions);

    let reloadMessageButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let reloadMessageButton = jqwidgets.createInstance('#reloadMessageButton', 'jqxButton', reloadMessageButtonOptions);

    let searchMessageButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let searchMessageButton = jqwidgets.createInstance('#searchMessageButton', 'jqxButton', searchMessageButtonOptions);
  }

  dropDownSource: string[] = [
    'Ид',
    'Текст',   
    'Отправитель',
    'Статус'
  ];

  findBtnOnClick(): void {
    this.messagesGrid.clearfilters();
    let searchColumnIndex = this.messagesFindDialogDropDownList.selectedIndex();
    let datafield = '';
    switch (searchColumnIndex) {
      case 0:
        datafield = 'id';
        break;
      case 1:
        datafield = 'msg_text';
        break;
      case 2:
        datafield = 'sender_info';
        break;
      case 3:
        datafield = 'status';
        break;   
    }
    let searchText = this.messagesFindDialogInput.val();
    let filtergroup = new jqx.filter();
    let filter_or_operator = 1;
    let filtervalue = searchText;
    let filtercondition = 'contains';
    let filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
    filtergroup.addfilter(filter_or_operator, filter);
    this.messagesGrid.addfilter(datafield, filtergroup);   
    this.messagesGrid.applyfilters();
  }

  clearBtnOnClick(): void {
    this.messagesGrid.clearfilters();
  }  
  
  messagesMediaFileRenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
    if (!value) {
      return '';
    }

    let url = this.config.apiEndpoint + 'api/audio_message/get_file/' + rowdata['id'];   

    let html =
      '<audio preload="auto" controls id="audio_' + rowdata['id'] + '">' +
      '  <source src="' + url + '">' +
      '</audio>';    

    let intervalId_ = setInterval(()=>{
      if ((window as any).initAudioPlayer) {
        (window as any).initAudioPlayer('#audio_' + rowdata['id']);
        clearInterval(intervalId_);
      }
    }, 500);

    return html;
  };

  messagesCreateDtRenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
    if (!value) {
      return '';
    }
   
    let formattedValue = moment(value).format('DD.MM.YYYY HH:mm:ss');
    return '<span style="margin: 9.5px 2px auto 4px; float: ' + columnproperties.cellsalign + '; color: black;">' + formattedValue + '</span>';
  }; 

  messagesColumns: any[] =
  [
    { text: 'Номер', datafield: 'id', width: 50, editable: false },
    { text: 'Дата и время', datafield: 'create_dt', width: 150, editable: false, cellsrenderer: this.messagesCreateDtRenderer },
    { text: 'Текст', datafield: 'msg_text', editable: false,
      cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
        if (newvalue == '') return oldvalue;

        if (newvalue == oldvalue) {
          let intervalId_ = setInterval(() => {
            if ((window as any).initAudioPlayerAll) {
              (window as any).initAudioPlayerAll();
              clearInterval(intervalId_);
            }
          }, 500);

          return newvalue;
        }
      }
    },  
    { text: 'Статус', datafield: 'status', width: 150, cellsalign: 'right', align: 'right', columntype: 'dropdownlist',   
      createeditor: (row: number, column: any, editor: any): void => {             
        editor.jqxDropDownList({ source: this.statusesNote, displayMember: 'name', valueMember: 'id' });
      },
      cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
        if (newvalue == '') return oldvalue;

        if (newvalue == oldvalue) {
          let intervalId_ = setInterval(() => {
            if ((window as any).initAudioPlayerAll) {
              (window as any).initAudioPlayerAll();
              clearInterval(intervalId_);
            }
          }, 500);

          return newvalue;
        }
      }
    },
    { text: 'Информация об отправителе', datafield: 'sender_info', width: 100, editable: false,
      cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
        if (newvalue == '') return oldvalue;

        if (newvalue == oldvalue) {
          let intervalId_ = setInterval(() => {
            if ((window as any).initAudioPlayerAll) {
              (window as any).initAudioPlayerAll();
              clearInterval(intervalId_);
            }
          }, 500);

          return newvalue;
        }
      }
    },
    { text: 'Медиафайл', datafield: 'file_name', sortable: false, filterable: false, editable: false, width: 250, cellsrenderer: this.messagesMediaFileRenderer }
  ];
   
  createMessagesDataAdapter(): any {
    let messagesSource: any =
    {
      datatype: 'json',
      datafields: [
        { name: 'id', type: 'int' },
        { name: 'status_id', type: 'int' },
        { name: 'status', value: 'status_id', values: { source: this.statusesNote.records, value: 'id', name: 'name' } },
        { name: 'sender_info', type: 'string' },
        { name: 'create_dt', type: 'string' },
        { name: 'status_dt', type: 'string' },
        { name: 'file_name', type: 'string' },
        { name: 'msg_text', type: 'string' },
        { name: 'sender_info', type: 'string' },
        { name: 'fserver_id', type: 'int' }
      ],
      root: '',
      record: '',     
      url: this.config.apiEndpoint + 'api/audio_message/get_messages',  
      updaterow: (rowid: number, rowdata: any): void => {
        // Обновление только статуса
        let messageId = rowdata['id'];
        let statusId = this.getStatusIdByName(rowdata['status']) || null;

        this.messagesHelperService.updateMessageStatus(messageId, statusId)
          .then(message => {
            alertify.success('Объект был успешно обновлен!');
          })
          .catch(err => {
            console.error(err);
            alertify.error(err.message);
          });
      }
    };
  
    return new jqx.dataAdapter(messagesSource); 
  }

  messagesDataAdapter: any = this.createMessagesDataAdapter();  

  readyMessagesGrid = (): void => {
    let intervalId = setInterval(() => {     
      if (
        document.querySelector('#addMessageButton')
        && document.querySelector('#deleteMessageButton')
        && document.querySelector('#reloadMessageButton')
        && document.querySelector('#searchMessageButton')
      ) {
        this.initMessagesButtons();
        clearInterval(intervalId);
      }
    }, 100);
  };

  nestedGrids: any[] = new Array();

  nestedGridInstances: any[] = new Array();

  nestedGridDataAdapters: any[] = new Array();

  currentRenderedNestedGrid: any = null;

  initRowDetails = (index: number, parentElement: any, gridElement: any, record: any): void => {    
    let nestedGridContainer = parentElement.children[0];
    this.nestedGrids[index] = nestedGridContainer;
   
    let url = this.config.apiEndpoint + 'api/audio_message/note/' + record.id;
    let adaptersSource = {
      datatype: 'json',
      datafields: [        
        { name: 'id', type: 'int' },
        { name: 'employee', type: 'string' },
        { name: 'dt', type: 'string' },        
        { name: 'note_text', type: 'string' },
        { name: 'message_id', type: 'int' }        
      ],    
      url: url,
      updaterow: (rowid: number, rowdata: any): void => {
        /*
        let messagesGridSelectRowIndex = this.messagesGrid.getselectedrowindex();
        let messagesGridRowsCount = this.messagesGrid.getdatainformation().rowscount;
        if (messagesGridSelectRowIndex >= 0 && messagesGridSelectRowIndex < parseFloat(messagesGridRowsCount)) {
          let messagesGridRowData = this.messagesGrid.getrowdata(messagesGridSelectRowIndex);
          let currentNestedGrid = this.nestedGridInstances[messagesGridSelectRowIndex];

          let currentNestedGridSelectedRowIndex = currentNestedGrid.getselectedrowindex();
          let currentNestedGridRowsCount = currentNestedGrid.getdatainformation().rowscount;
          if (currentNestedGridSelectedRowIndex >= 0 && currentNestedGridSelectedRowIndex < parseFloat(currentNestedGridRowsCount)) {
            let currentNestedGridRowId = currentNestedGrid.getrowid(currentNestedGridSelectedRowIndex);
            let currentNestedGridRowData = currentNestedGrid.getrowdata(currentNestedGridSelectedRowIndex);     

            const updatedNote = new Note();

            updatedNote.id = rowdata['id'];
            updatedNote.employee = rowdata['employee'];
            updatedNote.dt = rowdata['dt'];
            updatedNote.note_text = rowdata['note_text'];
            updatedNote.message_id = rowdata['message_id'];            

            if (updatedNote.isValid()) {            
              this.noteService.updateNote(messagesGridRowData['id'], updatedNote)
                .then(adapter => {
                  alertify.success('Объект был успешно обновлен!');
                })
                .catch(err => {
                  console.error(err);
                  alertify.error(err.message);
                });
            }
            else {
              alertify.error('Ошибка валидации объекта.');
            }            
          }
        }
        */          
      }
    }

    let nestedGridNote = new jqx.dataAdapter(adaptersSource);

    this.nestedGridDataAdapters[index] = nestedGridNote;

    if (nestedGridContainer != null) {
      let settings = {
          width: '100%',
          height: 200,
          editable: true,
          source: nestedGridNote, 
          columns: [
            { text: 'Сотрудник', datafield: 'employee', editable: false },
            { text: 'Текст', datafield: 'note_text', editable: false }
          ],
          showstatusbar: true,
          renderstatusbar: this.createNotesButtonsContainers,
          ready: this.readyNotesGrid
      };

      let newNestedGridInstance = jqwidgets.createInstance(`#${nestedGridContainer.id}`, 'jqxGrid', settings);
      this.currentRenderedNestedGrid = newNestedGridInstance;      
      this.nestedGridInstances[index] = newNestedGridInstance;
    }
  };

  rowdetailstemplate: any = {
    rowdetails: '<div id="nestedGrid" style="margin: 0px;"></div>',
    rowdetailsheight: 220,
    rowdetailshidden: true
  };

  readyNotesGrid = (): void => {   
    let prefix = this.currentRenderedNestedGrid.element.id;   

    let intervalId = setInterval(() => {     
      if (
        document.querySelector(`#${prefix}_addNoteButton`)
        && document.querySelector(`#${prefix}_deleteNoteButton`)
        && document.querySelector(`#${prefix}_reloadNoteButton`)
        && document.querySelector(`#${prefix}_searchNoteButton`)
      ) {
        this.initNotesButtons();
        clearInterval(intervalId);
      }
    }, 100);
  };

  initNotesButtons() {
    let prefix = this.currentRenderedNestedGrid.element.id;    
    let newEl = document.createElement('i');
    newEl.className = 'fas fa-plus-circle';
    let targetEl = document.querySelector(`#${prefix}_addNoteButton`);
    (targetEl as HTMLElement).dataset['parentId'] = prefix;
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let messagesGridSelectRowIndex = this.messagesGrid.getselectedrowindex();
      let messagesGridRowsCount = this.messagesGrid.getdatainformation().rowscount;
      if (messagesGridSelectRowIndex >= 0 && messagesGridSelectRowIndex < parseFloat(messagesGridRowsCount)) {
        let parentId = event.currentTarget.dataset['parentId'];
        let re = /[a-zA-Z]+(\d+)/;
        let parentIdParts = parentId.split(re);
        if (parentIdParts[1]) {
          let index = parseInt(parentIdParts[1]);

          if (messagesGridSelectRowIndex !== index) {
            alertify.alert('Информация', 'Выполнение действия невозможно для примечания другого сообщения, который не является активным.');
            return;
          }
        }

        let messagesGridRowData = this.messagesGrid.getrowdata(messagesGridSelectRowIndex);
        let currentNestedGrid = this.nestedGridInstances[messagesGridSelectRowIndex];     

        let newNote: Note = new Note();

        if (newNote.isValid()) {
          this.noteService.addNote(messagesGridRowData['id'], newNote)
            .then(note => {
              currentNestedGrid.addrow(null, newNote);
              currentNestedGrid.selectrow(currentNestedGrid.getdatainformation().rowscount - 1);

              alertify.success('Объект был успешно добавлен!');
            })
            .catch(err => {
              console.error(err);
              alertify.error(err.message);
            });
        }
        else {
          alertify.error('Ошибка валидации объекта.');
        }
      }
      else {
        alertify.alert('Информация', 'Действие может быть выполнено только для текущего сообщения. Сначала нужно выбрать строку, которая устанавливается как активный элемент.');
      }
    });

    newEl = document.createElement('i');
    newEl.className = 'fas fa-minus-circle';
    targetEl = document.querySelector(`#${prefix}_deleteNoteButton`);
    (targetEl as HTMLElement).dataset['parentId'] = prefix;
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let messagesGridSelectRowIndex = this.messagesGrid.getselectedrowindex();
      let messagesGridRowsCount = this.messagesGrid.getdatainformation().rowscount;
      if (messagesGridSelectRowIndex >= 0 && messagesGridSelectRowIndex < parseFloat(messagesGridRowsCount)) {
        let parentId = event.currentTarget.dataset['parentId'];
        let re = /[a-zA-Z]+(\d+)/;
        let parentIdParts = parentId.split(re);
        if (parentIdParts[1]) {
          let index = parseInt(parentIdParts[1]);

          if (messagesGridSelectRowIndex !== index) {
            alertify.alert('Информация', 'Выполнение действия невозможно для примечания другого сообщения, который не является активным.');
            return;
          }
        }

        let messagesGridRowData = this.messagesGrid.getrowdata(messagesGridSelectRowIndex);
        let currentNestedGrid = this.nestedGridInstances[messagesGridSelectRowIndex];

        let currentNestedGridSelectedRowIndex = currentNestedGrid.getselectedrowindex();
        let currentNestedGridRowsCount = currentNestedGrid.getdatainformation().rowscount;
        if (currentNestedGridSelectedRowIndex >= 0 && currentNestedGridSelectedRowIndex < parseFloat(currentNestedGridRowsCount)) {
          let currentNestedGridRowId = currentNestedGrid.getrowid(currentNestedGridSelectedRowIndex);
          let currentNestedGridRowData = currentNestedGrid.getrowdata(currentNestedGridSelectedRowIndex);     

          this.noteService.deleteNote(messagesGridRowData['id'], currentNestedGridRowData['id'])           
            .then(data => {
              alertify.success('Объект был успешно удален!');
            })
            .catch(err => {
              console.error(err);
              alertify.error(err.message);
            });

          currentNestedGrid.deleterow(currentNestedGridRowId);
        }
        else {
          alertify.alert('Информация', 'Действие может быть выполнено только для текущего примечания. Сначала нужно выбрать строку, которая устанавливается как активный адаптер.');
        }
      }
      else {
        alertify.alert('Информация', 'Действие может быть выполнено только для текущего сообщения. Сначала нужно выбрать строку, которая устанавливается как активный элемент.');
      }
    });
    (targetEl as HTMLElement).style.display = 'none';
    
    newEl = document.createElement('i');
    newEl.className = 'fas fa-sync-alt';
    targetEl = document.querySelector(`#${prefix}_reloadNoteButton`);
    (targetEl as HTMLElement).dataset['parentId'] = prefix;
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let messagesGridSelectRowIndex = this.messagesGrid.getselectedrowindex();
      let messagesGridRowsCount = this.messagesGrid.getdatainformation().rowscount;
      if (messagesGridSelectRowIndex >= 0 && messagesGridSelectRowIndex < parseFloat(messagesGridRowsCount)) {
        let parentId = event.currentTarget.dataset['parentId'];
        let re = /[a-zA-Z]+(\d+)/;
        let parentIdParts = parentId.split(re);
        if (parentIdParts[1]) {
          let index = parseInt(parentIdParts[1]);

          if (messagesGridSelectRowIndex !== index) {
            alertify.alert('Информация', 'Выполнение действия невозможно для примечания другого сообщения, который не является активным.');
            return;
          }
        }
        
        let messagesGridRowData = this.messagesGrid.getrowdata(messagesGridSelectRowIndex);
        let currentNestedGrid = this.nestedGridInstances[messagesGridSelectRowIndex];
        let currentNestedGridDataAdapter = this.nestedGridDataAdapters[messagesGridSelectRowIndex];

        currentNestedGrid.setOptions({source: currentNestedGridDataAdapter});
      }
    });
   
    newEl = document.createElement('i');
    newEl.className = 'fas fa-search';
    targetEl = document.querySelector(`#${prefix}_searchNoteButton`);
    (targetEl as HTMLElement).dataset['parentId'] = prefix;
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {    
    });

    (targetEl as HTMLElement).style.display = 'none';    
  }

  createNotesButtonsContainers(statusbar: any): void {
    let prefix = (this as any).element.id;
    
    let buttonsContainer = document.createElement('div');
    buttonsContainer.style.cssText = 'overflow: hidden; position: relative; margin: 5px;';

    let addNoteButtonContainer = document.createElement('div');
    let deleteNoteButtonContainer = document.createElement('div');
    let reloadNoteButtonContainer = document.createElement('div');
    let searchNoteButtonContainer = document.createElement('div');

    addNoteButtonContainer.id = `${prefix}_addNoteButton`;
    deleteNoteButtonContainer.id = `${prefix}_deleteNoteButton`;
    reloadNoteButtonContainer.id = `${prefix}_reloadNoteButton`;
    searchNoteButtonContainer.id = `${prefix}_searchNoteButton`;

    addNoteButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    deleteNoteButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    reloadNoteButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    searchNoteButtonContainer.style.cssText = 'float: left; margin-left: 5px;';

    buttonsContainer.appendChild(addNoteButtonContainer);
    buttonsContainer.appendChild(deleteNoteButtonContainer);
    buttonsContainer.appendChild(reloadNoteButtonContainer);
    buttonsContainer.appendChild(searchNoteButtonContainer);

    statusbar[0].appendChild(buttonsContainer);

    let addNoteButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'      
    }
    let addNoteButton = jqwidgets.createInstance(`#${prefix}_addNoteButton`, 'jqxButton', addNoteButtonOptions);

    let deleteNoteButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'
    }
    let deleteNoteButton = jqwidgets.createInstance(`#${prefix}_deleteNoteButton`, 'jqxButton', deleteNoteButtonOptions);

    let reloadNoteButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let reloadNoteButton = jqwidgets.createInstance(`#${prefix}_reloadNoteButton`, 'jqxButton', reloadNoteButtonOptions);

    let searchNoteButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let searchNoteButton = jqwidgets.createInstance(`#${prefix}_searchNoteButton`, 'jqxButton', searchNoteButtonOptions);
  }  

  currentMessage: any = null;

  currentMessageNotes: any[] = new Array();

  newNoteText: string;

  reloadNotes(messageId: number) {
    this.noteService.getNotes(messageId)
      .then(notes => {       
        this.currentMessageNotes = notes;
      })
      .catch(err => {
        console.error(err);
        alertify.error(err.message);
      });
  }

  messagesGridOnRowSelect(event: any): void {   
    let messagesGridRowData = this.messagesGrid.getrowdata(event.args.rowindex);
    this.currentMessage = messagesGridRowData;
    this.reloadNotes(messagesGridRowData['id']);
  }

  addNote(): void {
    if (!this.newNoteText || !this.newNoteText.length) {
      alertify.alert('Информация', 'Невозможно отправить пустое обсуждение.');
      return;
    }

    let newNote: Note = new Note();
    newNote.employee = 'test';
    newNote.note_text = this.newNoteText;

    this.noteService.addNote(this.currentMessage['id'], newNote)
      .then(note => {             
        this.reloadNotes(this.currentMessage['id']);
        this.newNoteText = '';
      })
      .catch(err => {
        console.error(err);
        alertify.error(err.message);
      });
  } 
}
