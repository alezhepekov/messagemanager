import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from './app.config';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': ''
  })
};

@Injectable({
  providedIn: 'root'
})
export class MessagesHelperService {

  constructor(    
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient) {   
  }

  getStatuses(): Promise<object> {
    const url = this.config.apiEndpoint + 'api/audio_message/status_list';
    return this.http.get<object>(url)
      .toPromise();    
  }

  updateMessageStatus(messageId, statusId) {
    const url = this.config.apiEndpoint + 'api/audio_message/status/' + messageId;
    const data = {
      status_id: statusId
    };
    return this.http.put<object>(url, data, httpOptions)
      .toPromise();
  }  
}
