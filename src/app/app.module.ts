import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { APP_CONFIG, APP_DI_CONFIG } from './app.config';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxinput';
import { jqxButtonComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';

import { MessagesHelperService } from './messageshelper.service';
import { NoteService } from './note.service';

@NgModule({
  declarations: [
    jqxGridComponent,
    jqxWindowComponent,
    jqxDropDownListComponent,
    jqxInputComponent,
    jqxButtonComponent,
    AppComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    InfiniteScrollModule
  ],
  providers: [
    { provide: APP_CONFIG, useValue: APP_DI_CONFIG },   
    MessagesHelperService,
    NoteService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
