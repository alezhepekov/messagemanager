export class Message {
  
  constructor() {
    this.id = null;
    this.status_id = null;
    this.sender_info = null;
    this.create_dt = null;
    this.status_dt = null;
    this.file_name = null;
    this.msg_text = null;
    this.status_employee = null;
    this.fserver_id = null;    
  }

  id: number;
  status_id: number;
  sender_info: string;
  create_dt: string;
  status_dt: string;
  file_name: string;
  msg_text: string;
  status_employee: string;
  fserver_id: number;

  isValid = (): boolean => {
    return true;
  }
}
