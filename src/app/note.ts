export class Note {

  constructor() {
    this.id = null;
    this.employee = null;
    this.dt = null;
    this.note_text = null;
    this.message_id = null;
  }

  id: number;
  employee: string;
  dt: string;
  note_text: string;
  message_id: number;

  isValid() {
    return this.note_text !== null && this.employee !== null;
  }
}
