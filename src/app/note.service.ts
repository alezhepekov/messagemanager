import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from './app.config';

import { Note } from './note';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': ''
  })
};

@Injectable({
  providedIn: 'root'
})
export class NoteService {

  constructor(    
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient) {   
  }

  getNotes(messageId: number): Promise<Note[]> {
    const url = this.config.apiEndpoint + 'api/audio_message/note/' + messageId;
    return this.http.get<Note[]>(url)
      .toPromise();
  }

  addNote(messageId: number, note: Note): Promise<Note> {
    const url = this.config.apiEndpoint + 'api/audio_message/note/' + messageId;
    return this.http.post<Note>(url, note, httpOptions)
      .toPromise();
  }

  updateNote(messageId: number, note: Note): Promise<Note> {
    const url = this.config.apiEndpoint + 'api/audio_message/note/' + messageId;
    return this.http.put<Note>(url, note, httpOptions)
      .toPromise();
  }

  deleteNote(messageId: number, id: number): Promise<object> {
    const url = this.config.apiEndpoint + 'api/audio_message/note/' + messageId;
    return this.http.delete<object>(url, httpOptions)
      .toPromise();
  }
}
